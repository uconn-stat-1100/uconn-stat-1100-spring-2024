# association demo plots

## positive association (home price vs. sqft)

sqft <- rnorm(1000, 2000, 500)

### clip houses below 1000sqft or above 3500sqft to make plot nicer
sqft <- sqft[(sqft >= 1000) & (sqft <= 3500)]

### make home prices
house_price <- rnorm(length(sqft), 40 * sqft + 80000, 20000)

### make plot and write it to disk
png("../figures/ch03/01.png", width = 866, height = 565)

plot(x = sqft, y = house_price,
     xlab = "House Size (SqFt)", ylab = "Sales Price ($)")

dev.off()


## negative association (engine displacement vs. fuel economy)

engine_displacement <- rnorm(1000, 250, 50)

### clip engines below 150 or above 350
engine_displacement <- engine_displacement[(engine_displacement >= 150) & (engine_displacement <= 350)]

### make fuel economies
fuel_economy <- rnorm(length(engine_displacement),
                      50 - 0.1 * engine_displacement,
                      4)

### make plot and write it to disk
png("../figures/ch03/02.png", width = 866, height = 565)

plot(x = engine_displacement, y = fuel_economy,
     xlab = "Engine Displacement (Cubic Inches)", ylab = "Fuel Economy (Miles/Gallon)")

dev.off()


## seasonal association (month of year vs. sales in an ice cream shop)

month <- 1:36

ice_cream_sales <- rnorm(length(month), 1500 + 1000 * sin(2 * pi * (month-4) / 12), 250)

### make plot and write it to disk
png("../figures/ch03/03.png", width = 866, height = 565)

plot(x = month, y = ice_cream_sales,
     xlab = "Month (1 = January 2017)",
     ylab = "Ice Cream Sold at a Shop (Pounds)")

dev.off()

# scatterplots


## intro: driver age vs. maximum distance at which highway sign is legible

driver_age <- read.csv('../data/ch03/driver_age.csv')


### plot of single point
png("../figures/ch03/04.png", width = 866, height = 565)

plot(x = driver_age[1,]$age, y = driver_age[1,]$distance,
     xlab = "Age",
     ylab = "Maximum Distance Highway Sign is Legible",
     xlim = c(16,85),
     ylim = c(260, 600))

dev.off()

### plot of whole dataset
png("../figures/ch03/05.png", width = 866, height = 565)

plot(x = driver_age$age, y = driver_age$distance,
     xlab = "Age",
     ylab = "Maximum Distance Highway Sign is Legible",
     xlim = c(16,85),
     ylim = c(260, 600))

dev.off()


## example of a point cloud

exam_scores <- rnorm(100, 85, 3)

temperature <- runif(100, 60, 80)

png("../figures/ch03/06.png", width = 866, height = 565)

plot(x = exam_scores, y = temperature,
     xlab = "Exam Score",
     ylab = "High Temperature the Day Before Exam")

dev.off()

# correlation

## quandrant explanation

quandrant_points <- data.frame("X" = c(1,1,2,2), "Y" = c(1,2,1,2))

png("../figures/ch03/07.png", width = 500, height = 500)

plot(quandrant_points, xaxt='n', yaxt='n')

abline(v=c(1.5), h=c(1.5))

text(1.42, 1.2, "Mean of X")

text(1.1, 1.55, "Mean of Y")

dev.off()


## correlation examples

### strong correlation
verbal_sat <- runif(1000, 400, 800)

college_gpa <- rnorm(length(verbal_sat), verbal_sat / 250, 0.25)

r <- cor(verbal_sat, college_gpa)

png("../figures/ch03/08.png", width = 866, height = 565)

plot(x = verbal_sat, y = college_gpa,
     xlab = "Verbal SAT Score",
     ylab = "College GPA",
     main = paste0("r: ", signif(r, 4)))

dev.off()


### weak correlation
age <- runif(1000, 16, 90)

tv <- abs(rnorm(length(age), age / 100, 4))

r <- cor(age, tv)

png("../figures/ch03/09.png", width = 866, height = 565)

plot(x = age, y = tv,
     xlab = "Age",
     ylab = "Hours of TV Watched (Daily)",
     main = paste0("r: ", signif(r, 4)))

dev.off()




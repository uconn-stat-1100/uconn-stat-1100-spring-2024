library(ggplot2)

# visualizations of PMFs

## flipping three fair coins, X = # of heads
pmf <- dbinom(c(0,1,2,3), 3, 0.5)

png("../figures/ch08/01.png", width = 500, height = 350)

barplot(pmf, names.arg=c(0,1,2,3), xlab = "Number of Heads", ylab="Probability")

dev.off()

## flipping a coin ten times
k <- 0:10

pmf <- dbinom(k, 10, 0.5)

png("../figures/ch08/02.png", width = 1000, height = 700)

barplot(pmf, names.arg=k, xlab = "Number of Heads", ylab="Probability")

dev.off()

# CDFs

## flipping three fair coins, X = # of heads, CDF at 4
k <- 0:10

pmf <- dbinom(k, 10, 0.5)

png("../figures/ch08/03.png", width = 1000, height = 700)

barplot(pmf, names.arg=k,
        xlab = "Number of Heads",
        ylab="Probability",
        col=c("black", "black", "black",
              "black", "black", "gray",
              "gray", "gray", "gray",
              "gray", "gray"))

dev.off()

## flipping three fair coins, X = # of heads, 5-to-8 interval
k <- 0:10

pmf <- dbinom(k, 10, 0.5)

png("../figures/ch08/04.png", width = 1000, height = 700)

barplot(pmf, names.arg=k,
        xlab = "Number of Heads",
        ylab="Probability",
        col=c("gray", "gray", "gray",
              "gray", "gray", "black",
              "black", "black", "black",
              "gray", "gray"))

dev.off()

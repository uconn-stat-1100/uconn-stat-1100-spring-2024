\documentclass[size=screen,aspectratio=169]{beamer}
\usepackage{beamerthemeshadow,amsmath,multimedia,graphicx,multicol,subfig,hyperref}
\begin{document}
\title{Chapter 03: Simple Linear Regression}
\author{Instructor: John Labarga}
\date{January 26, 2023}

\begin{frame}
\titlepage
\end{frame}

\begin{frame}
\frametitle{Table of Contents}
\tableofcontents
\end{frame} 

\section{Bivariate Relationships}
\subsection{Framework}
\begin{frame}
\frametitle{Related and Unrelated Pairs of Variables}
\begin{itemize}
\item Pairs of variables within a dataset may or may not be related.
\item Goal: build up a kit of tools for determining whether pairs of
  variables are related, and if so how strong their relationship is.
\item Example: your score on an exam
  \begin{itemize}
  \item Probably related: hours of sleep you got the night before,
    time spent studying the material in the week prior to the exam.
  \item Probably unrelated: inches of rainfall the day before the
    exam, score on an exam taken a week before in a different class.
  \end{itemize}
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Causal and Non-causal Relationships}
\begin{itemize}
\item Two variables have a \emph{causal} relationship if a change in
  the value of one directly induces a change in the value of the other
\item Causality can be in one direction or both directions
\item Tools we will learn do not assume that the relationships between
  the two variables is causal (i.e. they will work whether causality
  is present or not)
\end{itemize}
\end{frame}

\begin{frame}
    Example: causal relationship between sleep and exam performance

    \begin{itemize}

    
  \item You would expect that your number of hours spent sleeping the
    night before an exam has an effect on your performance
  \item If you sleep very little, you may struggle to focus and do
    poorly
  \item Up to a certain point you would expect that the more hours you
    sleep, the better you'll do on the exam.
  \item However you can also oversleep, causing you to miss the exam
    and score a 0
    
\end{itemize}
\end{frame}


\begin{frame}
    Example: non-causal relationship between math and reading SAT
    scores

    \begin{itemize}

    
  \item The purpose of the SAT is to measure college preparedness 
  \item Students who do better than average on one section tend to
    also do better than average on others (i.e. the scores have a
    positive relationship)
  \item The higher reading score does not cause a higher math score or
    vice versa
  \item This is a non-causal relationship: the variables are both
    caused by a third factor, general college preparedness, and don't
    directly cause each other
    
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Roles of Variables}
\begin{itemize}
\item In the bivariate modeling framework, one of the variables is the
  \textbf{response variable}, and the other is the \textbf{explanatory
    variable}.
\item The \textbf{response variable} is the one being modeled by the
  other variable. Often we want to model the response variable because
  it's hard or expensive to observe, or we want to know it in
  advance. Usually denoted \textbf{Y}.
\item The \textbf{explanatory variable} is the one ``explaining'' the
  response variable, and used to model the response variable. It's
  usually easily observable. Usually denoted \textbf{X}.
\item Example: modeling the future sales price of a house (response
  variable) using its square footage (explanatory variable).
  
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Types of Association}
\begin{itemize}
\item \textbf{Positive association}: as X increases, Y generally
  increases.
\item \textbf{Negative association}: as X increases, Y generally
  decreases.
\item Association is about the overall trend of the relationship
  between the two variables, it does not have to be strictly true for
  every pair of values.

  \item There will be variation even in cases of very strong
    association, leading to a scattering of points.
    
\end{itemize}

\end{frame}

\begin{frame}
\frametitle{Example: positive association}

\begin{figure}
  \centering
  \includegraphics[width=4in]{./figures/ch03/01.png}
\end{figure}

\end{frame}

\begin{frame}
\frametitle{Example: negative association}

\begin{figure}
  \centering
  \includegraphics[width=4in]{./figures/ch03/02.png}
\end{figure}

\end{frame}


\begin{frame}
\frametitle{Linear vs. Non-linear Relationships}
\begin{itemize}
\item Some variables have non-linear relationships
\item Examples:
  \begin{itemize}
  \item Quadratic (e.g. growth of a population of animals before hitting carrying capacity)
  \item Exponential (e.g. spread of an illness)
  \item Seasonal (e.g. electricity demand vs. time of day)
  \end{itemize}
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Example: seasonal association}

\begin{figure}
  \centering
  \includegraphics[width=4in]{./figures/ch03/03.png}
\end{figure}

\end{frame}



\subsection{Techniques}
\begin{frame}
\frametitle{Tools to Assess Bivariate Relationships}
\begin{itemize}
\item \textbf{Scatterplot}: a chart that plots each value of the first variable
  against each value of the second variable, visually displaying
  whether they tend to vary together
\item \textbf{Correlation coefficient}: a number that assess the strength of a
  potential linear relationship, giving the direction of the
  relationship and rating its strength from 0 to 1
\item \textbf{Regression equation}: a statistical model that gives the
  formula for a potential linear relationship, and comes equipped with
  a measurement ($R^2$) of the quality of the model's fit
\end{itemize}
\end{frame}


\section{Scatterplots}
\subsection{Setup}
\begin{frame}
  \frametitle{A Bivariate Dataset}

  \begin{center}
  Driver Age vs. Maximum Distance Highway Sign is Legible
  \end{center}
  
  \begin{table}[]
\begin{tabular}{ll|ll|ll}
Age                     & Distance & Age                     & Distance & Age                     & Distance \\ \hline
\multicolumn{1}{l|}{18} & 510      & \multicolumn{1}{l|}{37} & 420      & \multicolumn{1}{l|}{68} & 300      \\
\multicolumn{1}{l|}{20} & 590      & \multicolumn{1}{l|}{41} & 460      & \multicolumn{1}{l|}{70} & 390      \\
\multicolumn{1}{l|}{22} & 560      & \multicolumn{1}{l|}{46} & 450      & \multicolumn{1}{l|}{71} & 320      \\
\multicolumn{1}{l|}{23} & 510      & \multicolumn{1}{l|}{49} & 380      & \multicolumn{1}{l|}{72} & 370      \\
\multicolumn{1}{l|}{23} & 460      & \multicolumn{1}{l|}{53} & 460      & \multicolumn{1}{l|}{73} & 280      \\
\multicolumn{1}{l|}{25} & 490      & \multicolumn{1}{l|}{55} & 420      & \multicolumn{1}{l|}{74} & 420      \\
\multicolumn{1}{l|}{27} & 560      & \multicolumn{1}{l|}{63} & 350      & \multicolumn{1}{l|}{75} & 460      \\
\multicolumn{1}{l|}{28} & 510      & \multicolumn{1}{l|}{65} & 420      & \multicolumn{1}{l|}{77} & 360      \\
\multicolumn{1}{l|}{29} & 460      & \multicolumn{1}{l|}{66} & 300      & \multicolumn{1}{l|}{79} & 310      \\
\multicolumn{1}{l|}{32} & 410      & \multicolumn{1}{l|}{67} & 410      & \multicolumn{1}{l|}{82} & 360     
\end{tabular}
\end{table}

\end{frame}


\begin{frame}
\frametitle{Individual Point}

\begin{figure}
  \centering
  \includegraphics[width=4in]{./figures/ch03/04.png}
\end{figure}

\end{frame}

\begin{frame}
\frametitle{Whole Dataset}

\begin{figure}
  \centering
  \includegraphics[width=4in]{./figures/ch03/05.png}
\end{figure}

\end{frame}


\subsection{Usage}
\begin{frame}
\frametitle{Interpreting a Scatterplot}
\begin{itemize}
\item The overall relationship between the variables (if any) will be
  visible as a trend in the scatterplot
\item Some variation is expected, even very strong relationships
  won't form a perfect line
\item A ``cloud'' with no discernible trend indicates that there is
  probably no relationship between the two variables.
\end{itemize}
\end{frame}


\begin{frame}
\frametitle{Two Unrelated Variables}

\begin{figure}
  \centering
  \includegraphics[width=4in]{./figures/ch03/06.png}
\end{figure}

\end{frame}


\section{Correlation}
\subsection{Measuring the Direction and Strength of Association}
\begin{frame}
\frametitle{Desired Attributes of a Measure of Correlation}

\begin{itemize}
\item The measure should encode both the direction and strength of
  association
\item The measure should be ``unitless'', i.e. the measure should be
  comparable across two totally different datasets.
\item The measure should be confined to an interval, where one endpoint
  corresponds to perfect positive association, and the other
  corresponds to perfect negative association.
  \item The measure should reserve a special value corresponding to a
    total lack of association.
\end{itemize}

\end{frame}


\begin{frame}
\frametitle{The Pearson Correlation Coefficient}

  The most commonly-used measure of correlation is Pearson's
  correlation coefficient. Given a bivariate dataset with variables
  \textbf{X} and \textbf{Y}, where the $i^{\mbox{th}}$ row of the
  dataset contains $(X_i, Y_i)$, Pearson's correlation coefficient
  (commonly denoted by $r$) is:

  $$r = \frac{1}{n-1} \sum_{i=1}^n\frac{(X_i - \bar{X}) \cdot (Y_i - \bar{Y})}{S_X \cdot S_Y}$$
  
\end{frame}

\begin{frame}
\frametitle{Properties of Pearson's $r$}

\begin{itemize}
\item $r$ is a unitless quantity 
\item $-1 \leq r \leq 1$ for any dataset
\item $r$ is a measure of \emph{linear} association only
\item $r = -1$ means there is perfect negative linear association in the dataset
\item $r = 1$ means there is perfect positive linear association in the dataset
\item $r = 0$ means there is no linear association in the dataset
\end{itemize}

\end{frame}

\begin{frame}
\frametitle{Understanding the Correlation Coefficient}

\begin{itemize}
\item $r$ measures the strength and direction of linear association by
  comparing the values of $X_i$ and $Y_i$ to the means of their
  respective columns in the dataset ($\bar{X}$ and $\bar{Y}$).
\item Positive association will tend to have points with either $X_i >
  \bar{X}$ and $Y_i > \bar{Y}$, or $X_i < \bar{X}$ and $Y_i <
  \bar{Y}$. Both of these will result in \emph{positive} numerator
  values.
\item Negative association will tend to have points with either $X_i >
  \bar{X}$ and $Y_i < \bar{Y}$, or $X_i < \bar{X}$ and $Y_i >
  \bar{Y}$. Both of these will result in \emph{negative} numerator
  values.
\end{itemize}

\end{frame}

\begin{frame}
\frametitle{Understanding the Correlation Coefficient}

\begin{figure}
  \centering
  \includegraphics[width=2.5in]{./figures/ch03/07.png}
\end{figure}

\end{frame}

\begin{frame}
\frametitle{Calculating a Correlation Coefficient}

\begin{table}[]
\begin{tabular}{ll|ll|}
Exam 1 Score ($X$)           & Exam 2 Score ($Y$) &  $X_i - \bar{X}$                     & $Y_i - \bar{Y}$  \\ \hline
\multicolumn{1}{l|}{70} & 75           & \multicolumn{1}{l|}{} &  \\
\multicolumn{1}{l|}{75} & 82           & \multicolumn{1}{l|}{} &  \\
\multicolumn{1}{l|}{80} & 80           & \multicolumn{1}{l|}{} &  \\
\multicolumn{1}{l|}{80} & 86           & \multicolumn{1}{l|}{} &  \\
\multicolumn{1}{l|}{85} & 90           & \multicolumn{1}{l|}{} &  \\
\multicolumn{1}{l|}{90} & 91           & \multicolumn{1}{l|}{} & 
\end{tabular}
\end{table}

\end{frame}


\begin{frame}
\frametitle{Calculating a Correlation Coefficient}

\begin{table}[]
\begin{tabular}{ll|ll|}
Exam 1 Score ($X$)           & Exam 2 Score ($Y$) &  $X_i - \bar{X}$                     & $Y_i - \bar{Y}$  \\ \hline
\multicolumn{1}{l|}{70} & 75           & \multicolumn{1}{l|}{} &  \\
\multicolumn{1}{l|}{75} & 82           & \multicolumn{1}{l|}{} &  \\
\multicolumn{1}{l|}{80} & 80           & \multicolumn{1}{l|}{} &  \\
\multicolumn{1}{l|}{80} & 86           & \multicolumn{1}{l|}{} &  \\
\multicolumn{1}{l|}{85} & 90           & \multicolumn{1}{l|}{} &  \\
\multicolumn{1}{l|}{90} & 91           & \multicolumn{1}{l|}{} & 
\end{tabular}
\end{table}

First we need $\bar{X}$, $\bar{Y}$, $S_X$, and $S_Y$.

$\bar{X} = 80$, $\bar{Y} = 84$

$S_X = 7.07$, $S_Y = 6.16$

\end{frame}

\begin{frame}
\frametitle{Calculating a Correlation Coefficient}

\begin{table}[]
\begin{tabular}{ll|ll|}
Exam 1 Score ($X$)           & Exam 2 Score ($Y$) &  $X_i - \bar{X}$                     & $Y_i - \bar{Y}$  \\ \hline
\multicolumn{1}{l|}{70} & 75           & \multicolumn{1}{l|}{-10} & -9 \\
\multicolumn{1}{l|}{75} & 82           & \multicolumn{1}{l|}{-5} & -2 \\
\multicolumn{1}{l|}{80} & 80           & \multicolumn{1}{l|}{0} & -4 \\
\multicolumn{1}{l|}{80} & 86           & \multicolumn{1}{l|}{0} & 2 \\
\multicolumn{1}{l|}{85} & 90           & \multicolumn{1}{l|}{5} & 6 \\
\multicolumn{1}{l|}{90} & 91           & \multicolumn{1}{l|}{10} & 7
\end{tabular}
\end{table}

$\bar{X} = 80$, $\bar{Y} = 84$

$S_X = 7.07$, $S_Y = 6.16$

\end{frame}

\begin{frame}
\frametitle{Calculating a Correlation Coefficient}

\begin{table}[]
\begin{tabular}{ll|l}
$X_i - \bar{X}$        & $Y_i - \bar{Y}$ & $(X_i - \bar{X}) \cdot (Y_i - \bar{Y})$ \\ \hline
\multicolumn{1}{l|}{-10} & -9 & \multicolumn{1}{l|}{90} \\
\multicolumn{1}{l|}{-5}  & -2 & \multicolumn{1}{l|}{10} \\
\multicolumn{1}{l|}{0}   & -4 & \multicolumn{1}{l|}{0} \\
\multicolumn{1}{l|}{0}   & 2  & \multicolumn{1}{l|}{0} \\
\multicolumn{1}{l|}{5}   & 6  & \multicolumn{1}{l|}{30} \\
\multicolumn{1}{l|}{10}  & 7  & \multicolumn{1}{l|}{70}
\end{tabular}
\end{table}

$\bar{X} = 80$, $\bar{Y} = 84$

$S_X = 7.07$, $S_Y = 6.16$

\end{frame}


\begin{frame}
\frametitle{Calculating a Correlation Coefficient}

So we have:
$\sum_{i=1}^n (X_i - \bar{X}) \cdot (Y_i - \bar{Y}) = 200$

\vspace{5mm}
$\bar{X} = 80$, $\bar{Y} = 84$

\vspace{5mm}
$S_X = 7.07$, $S_Y = 6.16$

\vspace{5mm}
So $r$:

$$r \approx \frac{1}{5} \cdot \frac{200}{7.07 \cdot 6.16} \approx 0.918$$

\end{frame}

\subsection{Visual Association and Correlation}
\begin{frame}
\frametitle{What is correlation quantifying?}
\begin{itemize}
\item Correlation measures \emph{linear} association
\item The closer points are to lying on a line, the higher the
  correlation will be.
\item The more ``spread out'' points are from a plausible line, the
  lower the correlation will be.
  \item Outliers can reduce the correlation of an otherwise
    highly-associated pair of variables.
\end{itemize}

\end{frame}

\begin{frame}
\frametitle{Strong Correlation}

\begin{figure}
  \centering
  \includegraphics[width=4in]{./figures/ch03/08.png}
\end{figure}

\end{frame}


\begin{frame}
\frametitle{Weak Correlation}

\begin{figure}
  \centering
  \includegraphics[width=4in]{./figures/ch03/09.png}
\end{figure}

\end{frame}


\subsection{Pitfalls of Correlation}
\begin{frame}
\frametitle{Correlation and Non-linear Association}
\begin{itemize}
\item Correlation can only detect \emph{linear} association
\item Correlation will fail to detect non-linear association, even
  perfect non-linear association.
\item Generally the correlation coefficient of a non-linear
  association will not be exactly 0, since there will still generally
  be \emph{some} linear trend
  \item However, it's possible to construct cases of perfect
    non-linear association where $r=0$
\end{itemize}
\end{frame}


\begin{frame}
\frametitle{Example: Perfect association, yet $r=0$}
\begin{table}[]
\begin{tabular}{ll}
X                       & Y  \\ \hline
\multicolumn{1}{l|}{-5} & 25 \\
\multicolumn{1}{l|}{-3} & 9  \\
\multicolumn{1}{l|}{-2} & 4  \\
\multicolumn{1}{l|}{0}  & 0  \\
\multicolumn{1}{l|}{2}  & 4  \\
\multicolumn{1}{l|}{3}  & 9  \\
\multicolumn{1}{l|}{5}  & 25
\end{tabular}
\end{table}\end{frame}

\section{Regression}
\subsection{Introduction}
\begin{frame}
\frametitle{Motivation}
\begin{itemize}
\item When two variables are associated, we want to go further than
  just quantifying the strength of that association
\item Also often want a model of the response variable in terms of the
  explanatory variable
\item Example: house price vs. square feet. We want a model that
  allows us to predict the sales price of a house once we know the
  house's square footage.
\end{itemize}
\end{frame}


\begin{frame}
  \frametitle{The Simple Linear Regression Model}

  $$\hat{Y} = b_0 + b_1X$$

  $\hat{Y}$ is a model of $Y$ ( the response variable) in terms of $X$
  (the explanatory variable).

  \vspace{5mm}
  The model is a line with slope $b_1$ and intercept $b_0$.
  
\end{frame}


\begin{frame}
\frametitle{Fitting Our First Regression: Data}
\begin{table}[]
\begin{tabular}{ll}
Age                    & Vocab \\ \hline
\multicolumn{1}{l|}{1} & 10    \\
\multicolumn{1}{l|}{2} & 500   \\
\multicolumn{1}{l|}{3} & 1000  \\
\multicolumn{1}{l|}{4} & 1500  \\
\multicolumn{1}{l|}{5} & 2000  \\
\multicolumn{1}{l|}{6} & 2400 
\end{tabular}
\end{table}
\end{frame}


\begin{frame}
\frametitle{Fitting Our First Regression: Prep}

You only need to perform the following series of steps each time you
reset your calculator. If you never do so, you only need to do this
once.

\begin{enumerate}
\item Press 2nd and the ‘0’ key
\item Scroll through the alphabetical listings until you find DIAGNOSTIC ON
\item Press ENTER twice and the set up is complete
\end{enumerate}

\end{frame}

\begin{frame}
\frametitle{Fitting Our First Regression: Fitting the Model}
\begin{enumerate}
\item Enter the X values into L1 and the Y values into L2
\item Set up Plot1 for the Scatterplot
\item Press STAT and move the cursor to CALC
\item Scroll to 8:LinReg(a+bx) and press ENTER
\item With the cursor flashing next to LinReg(a+bx), press 2nd and ‘1’
  followed by a comma.  Press 2nd and ‘2’ followed by a comma.  Press
  VARS, move the cursor to Y-VARS, press ENTER to select 1:Function
  and press ENTER to select Y1
\item Press ENTER to see the regression results
\item Press Zoom and press ‘9’ to see the graph
\end{enumerate}
\end{frame}


\subsection{Model Interpretation and Use}
\begin{frame}
\frametitle{Interpreting the Intercept and Slope}
\begin{itemize}
\item Predictions can be extracted from the model by plugging in a
  value for $X$ and getting the corresponding $\hat{Y}$
\item The intercept $b_0$ is the model's prediction when $X = 0$; it
  may or may not be interpretable
\item The slope $b_1$ is the predicted increase in $Y$ when $X$ increases by 1
\end{itemize}
\end{frame}


\begin{frame}
\frametitle{Example: City Latitude and Mean August Temperature}
\begin{table}[]
\begin{tabular}{lll}
City                                  & Latitude                & Mean August Temperature \\ \hline
\multicolumn{1}{l|}{Miami FL}         & \multicolumn{1}{l|}{26} & 83                      \\
\multicolumn{1}{l|}{Houston TX}       & \multicolumn{1}{l|}{30} & 82                      \\
\multicolumn{1}{l|}{Mobile AL}        & \multicolumn{1}{l|}{31} & 82                      \\
\multicolumn{1}{l|}{Phoenix AZ}       & \multicolumn{1}{l|}{33} & 92                      \\
\multicolumn{1}{l|}{Dallas TX}        & \multicolumn{1}{l|}{33} & 85                      \\
\multicolumn{1}{l|}{Los Angeles CA}   & \multicolumn{1}{l|}{34} & 75                      \\
\multicolumn{1}{l|}{Memphis TN}       & \multicolumn{1}{l|}{35} & 81                      \\
\multicolumn{1}{l|}{Norfolk VA}       & \multicolumn{1}{l|}{37} & 77                      \\
\multicolumn{1}{l|}{San Francisco CA} & \multicolumn{1}{l|}{38} & 64                      \\
\multicolumn{1}{l|}{Baltimore MD}     & \multicolumn{1}{l|}{39} & 76                      \\
\multicolumn{1}{l|}{Kansas City MO}   & \multicolumn{1}{l|}{39} & 74                      \\
\multicolumn{1}{l|}{Washington DC}    & \multicolumn{1}{l|}{39} & 76                     
\end{tabular}
\end{table}
\end{frame}


\begin{frame}
  \frametitle{Example: City Latitude and Mean August Temperature}

  $$\hat{\mbox{Temp}} = 113.69 - 1.008 \cdot \mbox{Latitude}$$

  Interpretation:
  \begin{itemize}
  \item $b_0 = 113.69 \Rightarrow $ predicted mean August temperature at equator is $113.69^{\circ}$F (probably not a good prediction)
  \item $b_1 = -1.008 \Rightarrow $ a city is predicted to be about $1.008^{\circ}$F cooler in August for each degree of latitude further north of the equator it is. 
  \end{itemize}
  
  
\end{frame}


\begin{frame}
  \frametitle{Making Predictions with Regression Models}

  Suppose we wanted to estimate the mean August temperature for Boston
  MA, which was not in our dataset. We would need Boston's latitude,
  which we could plug into the regression model to extract its
  predicted mean August temperature.

  Suppose that Boston's latitude is $42$ degrees north of the
  equator. Plugging this into the regression model:

  $$\hat{\mbox{Boston}} = 113.69 - 1.008 \cdot 42 = 71.354$$
  
\end{frame}

\begin{frame}
  \frametitle{Relationship Between Correlation and Regression}

  In simple linear regression, the correlation coefficient $r$ and the
  regression model $\hat{Y} = b_0 + b_1X$ are closely related:

  $$b_1 = r \cdot \frac{S_Y}{S_X}$$

  $$b_0 = \bar{Y} - b_1\bar{X}$$

  Since the sample standard deviations are always positive, this means
  that the slope of the regression line must have the same sign as the
  correlation coefficient.
  
\end{frame}


\subsection{Quality of Fit}
\begin{frame}
  \frametitle{Regression Quality at a Single Point}

  A good regression should yield a predicted value relatively close to
  the actual value, i.e. $\hat{Y}_i$ should be close to $Y_i$.

  \vspace{5mm}

  We can measure how much the regression ``missed'' a given point by
  the vertical distance between the actual value and the predicted
  value. This distance is called the \emph{residual}:

  $$\mbox{Residual}_i = e_i = Y_i - \hat{Y}_i$$
  
\end{frame}


\begin{frame}
  \frametitle{Residual Example}

  Our regression for mean August temperature in terms of latitude
  predicted that Boston's mean August temperature is $71.354$ degrees.

  \vspace{5mm}

  If Boston's mean August temperature is actually $72$
  degrees, then the residual at this point is $72 - 71.354 = 0.646$
  degrees, i.e. the model is off by less than 1 degree for Boston.
  
\end{frame}


\begin{frame}
  \frametitle{How is the ``best'' regression line chosen?}

  For a given dataset, a variety of regression lines could be chosen
  that model the data ``best''. We need a definition of what ``best''
  means in this context.

  \vspace{5mm}

  The ``best'' regression line is the one that minimizes some ``total
  error'', i.e. given some measure of how ``wrong'' the model is, we
  want to be the ``least wrong''.
  
\end{frame}


\begin{frame}
  \frametitle{Candidate 1}

  Why not use the total residual? I.e.

  $$\sum_{i=1}^n e_i = \sum_{i=1}^n Y_i - \hat{Y}_i$$

  It turns out that if you sum the residuals up, the sum is always 0
  (try this for yourself).

  \vspace{5mm}

  This is analogous to when we tried to measure the dispersion of a
  dataset in chapter 02 as the sum of the distances from the
  mean. That quantity also always totaled to 0.
  
\end{frame}


\begin{frame}
  \frametitle{Candidate 2}

  Let's solve this problem the same way we did in chapter 02. Consider
  the squared residuals instead:

  $$\sum_{i=1}^n (e_i)^2 = \sum_{i=1}^n (Y_i - \hat{Y}_i)^2$$

  This is guaranteed to be at least 0 (and is only 0 if the fit is
  perfect). Notice that since:

  $$\sum_{i=1}^n (e_i)^2 = \sum_{i=1}^n (Y_i - \hat{Y}_i)^2 = \sum_{i=1}^n (Y_i - (b_0 + b_1X_i))^2$$

  There is one unique $(b_0, b_1)$ combination that minimizes this sum
  of squared residuals.

\end{frame}

\begin{frame}
  \frametitle{The ``Least Squares'' Regression Model}

  In fact, this is how the regression model is computed. Statistical
  software finds the $(b_0, b_1)$ combination that minimizes the
  quantity:

  $$\sum_{i=1}^n (e_i)^2 = \sum_{i=1}^n (Y_i - \hat{Y}_i)^2 = \sum_{i=1}^n (Y_i - (b_0 + b_1X_i))^2$$

  This is called the ``least squares'' regression model, and by this
  measure there is just one best model for each dataset.

\end{frame}

\begin{frame}
  \frametitle{Toward a Quality of Fit on the Whole Dataset}

  Suppose you had no regression model but still had to predict the $Y$
  value of a new point. The best you could do would be to take the
  average of all $Y$ values, $\bar{Y}$, and use it as your
  prediction. Then your error at point $Y_i$ would be:

  $$Y_i - \bar{Y}$$

  and your total squared error (aka total sum of squares) would be:

  $$\sum_{i=1}^n (Y_i - \bar{Y})^2$$

\end{frame}


\begin{frame}
  \frametitle{Toward a Quality of Fit on the Whole Dataset}

  Convenient property of least squares regression:

  $$\sum_{i=1}^n (Y_i - \bar{Y})^2 = \sum_{i=1}^n ((Y_i - \hat{Y}_i) + (\hat{Y}_i - \bar{Y}))^2$$

  $$ = \sum_{i=1}^n (Y_i - \hat{Y}_i)^2 + \sum_{i=1}^n(\hat{Y}_i - \bar{Y})^2$$

  \vspace{5mm} \emph{Note}: this is a special property of least
  squares regression. In general, $(a + b)^2 \neq a^2 + b^2$; this is
  a fallacy called the
  \href{https://en.wikipedia.org/wiki/Freshman\%27s_dream}{freshman's
    dream}.

\end{frame}

\begin{frame}
  \frametitle{Components of the Total Error}

  $\sum_{i=1}^n(\hat{Y}_i - \bar{Y})^2$: explained sum of squares (SSR)

  \vspace{5mm}
  $\sum_{i=1}^n(Y_i - \hat{Y}_i)^2$: unexplained sum of squares (SSE)

  \vspace{5mm}
  $\sum_{i=1}^n(Y_i - \bar{Y})^2$: total sum of squares (SSTO)

  \vspace{5mm}

  $$\mbox{SSTO} = \mbox{SSE} + \mbox{SSR}$$

\end{frame}


\begin{frame}
  \frametitle{Quality of Fit Metric: $R^2$}

  SSR is how much of the error our regression model has ``explained'',
  i.e. reduced.

  \vspace{5mm}
  SSE is how much of the error remains ``unexplained''.

  \vspace{5mm}
  Our quality of fit metric is the proportion of error explained by the regression model:

  $$R^2 = \frac{SSR}{\mbox{SSTO}} = 1 - \frac{SSE}{\mbox{SSTO}}$$

\end{frame}

\begin{frame}
  \frametitle{What is $R^2$?}

  Formally, $R^2$ (called the \emph{coefficient of determination}) is
  the proportion of the total error that has been explained by the
  regression.

  \vspace{5mm}

  $R^2$ is the proportion of the variance of the response variable
  explained by the explanatory variable.

  \vspace{5mm}

  Put another way, this corresponds to the proportional reduction in
  uncertainty about the response variable that we get from using a
  model built on the explanatory variable.

\end{frame}

\begin{frame}
  \frametitle{Properties of $R^2$}

  \begin{itemize}
    \item Since SSTO, SSE, and SSR are all positive, $R^2$ is at least
      0 and at most 1.

    \item In simple linear regression, $R^2 = r^2$; i.e. the
      coefficient of determination is the correlation coefficient
      squared.

    \item $R^2$ is 0 if and only if there is no linear relationship
      between the variables.

    \item $R^2$ is 1 if and only if there is a perfect linear
      relationship between the variables.

    \item $R^2$ is unitless, and can be compared across regressions.

        
  \end{itemize}

\end{frame}

\subsection{Regression difficulties and remedies}
\begin{frame}
  \frametitle{Extrapolation}

  Extrapolation is the use of a model outside the $X$ range that it
  was fit on.

  \vspace{5mm}

  Recall the temperature-latitude model:

  $$\hat{\mbox{Temp}} = 113.69 - 1.008 \cdot \mbox{Latitude}$$

  This model says that the mean August temperature at the equator is
  $113.69$ degrees, and the mean August temperature at the North Pole
  is $22.97$ degrees.

  \vspace{5mm}

  In reality, Singapore's mean August temperature is 88.5 degrees, and
  Nairobi's is 73.4 degrees. The mean August temperature at the north
  pole is 64.4 degrees.
  
\end{frame}

\begin{frame}
  \frametitle{Influence of Outliers}

  \begin{itemize}
    \item Outliers can be present in the bivariate case, just as they
      could be present in the univariate case (and for the same
      reasons).

    \item Outliers will generally reduce $R^2$, and may change the
      entire direction of association.

    \item Outliers should \textbf{not} be removed simply because they
      reduce the quality of fit. Exclusion should be based on whether
      the observation is from another population, or is an error of
      some kind.
        
  \end{itemize}

  
\end{frame}


\begin{frame}
  \frametitle{Influence of Outliers: Example}

  \vspace{-3mm}
  
  \begin{table}[]
\begin{tabular}{l|l}
Magnitude & Deaths \\ \hline
7.9       & 1      \\
6.8       & 30     \\
7.4       & 27     \\
6.6       & 60     \\
7.8       & 503    \\
6.2       & 115    \\
7.1       & 9      \\
7.1       & 8      \\
7.3       & 12     \\
7.3       & 28     \\
6.6       & 65     \\
6.9       & 62     \\
7.3       & 3      \\
6.7       & 60    
\end{tabular}
\end{table}
  
\end{frame}


\begin{frame}
  \frametitle{Fitting a Linear Model to Non-Linear Data}

  \begin{itemize}
    \item Real-world datasets frequently have multiple patterns in
      them, including a linear pattern.

    \item A linear model will not model non-linear data well; more
      complex models (polynomial, seasonal, etc.) are necessary.

    \item To determine if there are non-linear patterns in a dataset,
      compute the residuals of a linear regression on that
      data. Non-linear patterns in the data will persist in this
      residual plot.
        
  \end{itemize}

\end{frame}


\begin{frame}
\frametitle{Example: fitting a linear regression to non-linear data}
\begin{table}[]
\begin{tabular}{ll}
X                       & Y  \\ \hline
\multicolumn{1}{l|}{-5} & 25 \\
\multicolumn{1}{l|}{-3} & 9  \\
\multicolumn{1}{l|}{-2} & 4  \\
\multicolumn{1}{l|}{0}  & 0  \\
\multicolumn{1}{l|}{2}  & 4  \\
\multicolumn{1}{l|}{3}  & 9  \\
\multicolumn{1}{l|}{5}  & 25
\end{tabular}
\end{table}\end{frame}



\begin{frame}
  \frametitle{Correlation vs. Causation}

  \begin{itemize}
    \item Always keep in mind that correlation is only measuring the
      strength and direction of linear association between two
      variables.

    \item A strong correlation does not imply that the relationship is
      causal.

    \item Nor does a weak correlation imply the absence of a causal
      relationship.
        
  \end{itemize}

  
\end{frame}




\end{document}

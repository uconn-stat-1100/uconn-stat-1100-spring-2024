\documentclass[size=screen,xcolor=table,aspectratio=169]{beamer}
\usepackage{beamerthemeshadow,amsmath,multimedia,graphicx,multicol,multirow,mathtools}
\begin{document}
\title{Chapter 10: Confidence Intervals for the Population Proportion}
\author{Instructor: John Labarga}
\date{April 04, 2023}

\begin{frame}
\titlepage
\end{frame}

\begin{frame}
\frametitle{Table of Contents}
\tableofcontents
\end{frame} 

\section{Single Sample Case}

\subsection{Sampling Distributions to Confidence Intervals}

\begin{frame}
  \frametitle{Using a Random Variable as a Point Estimate}

  Using the central limit theorem (CLT), we were able to show that the
  sampling distribution of $\hat{p}$ is:

  $$N \left(p, \sqrt{\frac{p(1-p)}{n}} \right)$$

  \vspace{3mm}

  This means that the average value of $\hat{p}$ is $p$, so $\hat{p}$
  can be used to estimate $p$. We call $\hat{p}$ a \emph{point
    estimator} of $p$.

  \vspace{3mm}

  But $\hat{p}$ is still a random variable, so it will vary around
  $p$. For some samples you may get a very close estimate of $p$, but
  for others the estimate may be less accurate.

\end{frame}

\begin{frame}
  \frametitle{Building a Better Estimate of $p$ from $\hat{p}$}

  Since $\hat{p}$ is a continuous random variable (for sufficiently
  large samples), $P(\hat{p} = p)=0$. So the estimate of $p$ gotten from
  $\hat{p}$ may be close, but it will never be exactly right.

  \vspace{3mm}

  \underline{Idea}: instead of using a single-value estimate, which we
  know will be off by some amount, let's use the sampling distribution
  to build an interval from $\hat{p}$ that contains $p$ with some
  probability (confidence).

  \vspace{3mm}

  This interval is called a \emph{confidence interval}.

\end{frame}

\subsection{Constructing a Confidence Interval for $p$}

\begin{frame}
  \frametitle{Logic of Confidence Intervals}

  A \emph{confidence interval} is an interval $[a,b]$, constructed
  only from sample statistics, that contains the unknown population
  parameter with a certain \emph{confidence}.

  \vspace{3mm}

  We want a procedure such that, given a confidence level X, an
  interval built with this procedure contains the population parameter
  X\% of the time.

  \vspace{3mm}

  Any individual confidence interval will either contain the
  population parameter or not (though we will not know whether it
  did), but if we could draw a large number of samples we would see
  that the parameter was in about X\% of the interval and outside of
  about (100-X)\% of them.

\end{frame}


\begin{frame}
  \frametitle{Intervals Under the Sampling Distribution}

  For a fixed population proportion $p$, the distribution of $\hat{p}$
  is $$N \left(p, \sqrt{\frac{p(1-p)}{n}} \right)$$

  \vspace{3mm}

  Using the empirical rule, this means that there is a 95\% chance of
  $\hat{p}$ being between $p - 2\sqrt{\frac{p(1-p)}{n}}$ and $p +
  2\sqrt{\frac{p(1-p)}{n}}$. So the interval:

  $$\left [p - 2\sqrt{\frac{p(1-p)}{n}}, p +
    2\sqrt{\frac{p(1-p)}{n}} \right]$$

  contains $\hat{p}$ with \emph{95\% probability}.

\end{frame}


\begin{frame}
  \frametitle{Relaxing the Assumption of Known $p$}

  In most applied settings, we're not trying to learn about $\hat{p}$
  from $p$ but rather the reverse: we lack information about the
  population proportion $p$, and have a concrete $\hat{p}$ value from
  a sample.

  \vspace{3mm}

  Given a sufficiently large sample, we can substitute the standard
  error in for the standard deviation of the sampling distribution,
  giving $p$ the distribution:

  $$N \left(p, \sqrt{\frac{\hat{p}(1-\hat{p})}{n}} \right)$$

\end{frame}

\begin{frame}
  \frametitle{Building a Confidence Interval}

  Suppose we could find a number $z$ such that, for an arbitrary
  confidence level CL we had:

  $$P \left(p - z \sqrt{\frac{\hat{p}(1-\hat{p})}{n}} \leq \hat{p}
    \leq p + z \sqrt{\frac{\hat{p}(1-\hat{p})}{n}} \right) =
  \mbox{CL}$$

  After rearranging some terms, this is the same as:

  $$P \left(\hat{p} - z \sqrt{\frac{\hat{p}(1-\hat{p})}{n}} \leq p
    \leq \hat{p} + z \sqrt{\frac{\hat{p}(1-\hat{p})}{n}} \right) =
  \mbox{CL}$$

\end{frame}

\begin{frame}
  \frametitle{Building a Confidence Interval}

  So if we can identify this ``multiplier'' $z$, the interval:

  $$\left[\hat{p} - z \sqrt{\frac{\hat{p}(1-\hat{p})}{n}},
  \hat{p} + z \sqrt{\frac{\hat{p}(1-\hat{p})}{n}} \right]$$

  Contains the unknown population parameter $p$ with confidence CL
  (e.g. 80\%, 90\%, 95\%, etc.).

  \vspace{3mm}
  
  This is our confidence interval for
  $p$, built only with statistics from the sample ($\hat{p}$ and
  $n$). It's composed of three components: the point estimate, the
  multiplier, and the standard error.

\end{frame}

\begin{frame}
  \frametitle{Finding the Multiplier $z$}

  Since the distribution of $\hat{p}$ is:

  $$N \left(p, \sqrt{\frac{\hat{p}(1-\hat{p})}{n}} \right)$$

  The multiplier $z$ is just a percentile of the $N(0,1)$
  distribution such that CL\% of the area under the $N(0,1)$ curve is
  between $-z$ and $+z$.

  \vspace{3mm}

  This can be found on the calculator with:

  $$\mbox{invNorm}\left(\frac{1 - \mbox{CL}}{2}, 0, 1 \right)$$

\end{frame}

\begin{frame}
  \frametitle{Computing Confidence Intervals}

  Though confidence intervals for $p$ can be found directly with the
  formula, functions in statistical software (and our calculators) can
  compute them more easily.

  \vspace{3mm}

  To calculate a one-sample confidence interval for $p$ on the
  TI-83/84, use:

  $$\mbox{STAT} \rightarrow \mbox{Tests} \rightarrow \mbox{1-PropZInterval}$$

\end{frame}

\subsection{Examples}

\begin{frame}
  \frametitle{Example: Support for a Political Candidate}

  A pollster runs a poll the week before an election between two
  candidates, asking 450 respondents the question ``do you plan to
  vote for candidate A?'' 260 of them say yes.

  \vspace{3mm}

  Build 90\%, 95\%, and 99\% confidence intervals for $p$, the
  proportion of the population that supports candidate A.

\end{frame}

\begin{frame}
  \frametitle{Example: Support for a Political Candidate}

  90\% confidence interval:

  $$[0.53948, 0.61608]$$

  \vspace{3mm}

  95\% confidence interval:

  $$[0.53214, 0.62341]$$

  \vspace{3mm}

  99\% confidence interval:

  $$[0.5178, 0.63775]$$

\end{frame}

\begin{frame}
  \frametitle{Example: Popularity of an Experience}

  In a survey of 1500 US adults, 341 had traveled internationally.

  \vspace{4mm}

  Build a 98\% confidence interval for $p$, the proportion of all US
  adults who have traveled internationally.

  \vspace{20mm}

\end{frame}

\begin{frame}
  \frametitle{Example: Popularity of an Experience}

  In a survey of 1500 US adults, 341 had traveled internationally.

  \vspace{4mm}

  Build a 98\% confidence interval for $p$, the proportion of all US
  adults who have traveled internationally.

  \vspace{3mm}

  98\% confidence interval:

  $$[0.20216, 0.25251]$$  

\end{frame}

\begin{frame}
  \frametitle{Factors that Affect the Width of a Confidence Interval}

  \begin{itemize}

    \item Confidence level: for a fixed sample, a higher confidence
      level will result in a wider interval.

    \item Sample size: for a fixed confidence level, a larger sample
      size will result in a narrower interval.

    \item Value of $\hat{p}$: if $\hat{p}$ is close to 0 or 1, the
      standard error of the sampling distribution will be smaller than
      if $\hat{p}$ is near 0.5. This yields a narrower confidence
      interval. 


  \end{itemize}

\end{frame}


\section{Two Sample Case}

\subsection{Constructing a Confidence Interval for $p_1 - p_2$}

\begin{frame}
  \frametitle{Idea of a Two-Sample Confidence Interval}

  In some applications, the difference (or lack thereof) between two
  population proportions is of interest.

  \vspace{5mm}

  Examples:

  \begin{itemize}

  \item Comparison of voting habits between two counties or states.

  \item Comparison of educational attainment between two countries.

  \item Comparison of health status between two groups of people.

  \end{itemize}

\end{frame}

\begin{frame}
  \frametitle{Sampling Distribution of $\hat{p}_1 - \hat{p}_2$}

  The parameter we're trying to estimate with out confidence interval
  is the difference between the two population parameters: $p_1 -
  p_2$.

  \vspace{3mm}

  The point estimate for this quantity is $\hat{p}_1 - \hat{p}_2$,
  which has the following sampling distribution:


  $$N \left(p_1 - p_2, \sqrt{\frac{\hat{p}_1(1-\hat{p}_1)}{n_1} +
      \frac{\hat{p}_2(1-\hat{p}_2)}{n_2}} \right)$$

  \vspace{3mm}

  where $n_1$ and $n_2$ are the sample sizes of sample 1 and 2.

\end{frame}

\begin{frame}
  \frametitle{Confidence Interval for $p_1 - p_2$}

  This yields a confidence interval of:


  $$\left[ \hat{p}_1 - \hat{p}_2 \pm z\sqrt{\frac{\hat{p}_1(1-\hat{p}_1)}{n_1} +
      \frac{\hat{p}_2(1-\hat{p}_2)}{n_2}} \right]$$

  \vspace{3mm}

  where $z$ is a standard normal multiplier.

  \vspace{3mm}

  On the calculator:

  $$\mbox{STAT} \rightarrow \mbox{Tests} \rightarrow \mbox{2-PropZInterval}$$

\end{frame}

\begin{frame}
  \frametitle{Conditions for the Confidence Interval for $p_1 - p_2$}

  The central limit theorem is required for the sampling distribution
  of $\hat{p}_1 - \hat{p}_2$. The following conditions must hold for
  it to be usable:

  \begin{itemize}

  \item $n_1$ and $n_2$ must each be at least 30

  \item $x_1$ and $x_2$ must each be at least 10

  \item $n_1 - x_1$ and $n_2 - x_2$ must each be at least 10

  \end{itemize}
  

\end{frame}


\subsection{Example}

\begin{frame}
  \frametitle{Example: Seatbelt Adherence}

  In a survey of US high school seniors, a group of males and females
  were asked whether they always wore a seatbelt when driving. Of the
  1467 females, 915 said they always wore a seatbelt when driving. If
  the 1575 males, 771 said they always wore a seatbelt when driving.

  \vspace{4mm}

  Build a 95\% confidence interval for $p_F-p_M$, the difference in
  seatbelt use between females and males.

  \vspace{20mm}


\end{frame}


\begin{frame}
  \frametitle{Example: Seatbelt Adherence}

  In a survey of US high school seniors, a group of males and females
  were asked whether they always wore a seatbelt when driving. Of the
  1467 females, 915 said they always wore a seatbelt when driving. If
  the 1575 males, 771 said they always wore a seatbelt when driving.

  \vspace{4mm}

  Build a 95\% confidence interval for $p_F-p_M$, the difference in
  seatbelt use between females and males.

  \vspace{3mm}

  95\% confidence interval for $p_F - p_M$:

  $$[0.09921, 0.16918]$$


\end{frame}


\end{document}
